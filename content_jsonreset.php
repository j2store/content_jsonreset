<?php
/**
 * --------------------------------------------------------------------------------
 * Content Plugin - Content Json reset
 * --------------------------------------------------------------------------------
 * @package     Joomla 2.5 -  3.x
 * @subpackage  Content
 * @author      Alagesan,
 * @copyright   Copyright (c) 2016 J2Store . All rights reserved.
 * @license     GNU/GPL license: http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
defined('_JEXEC') or die('Restricted access');
class plgContentContent_jsonreset extends JPlugin
{
    var $_element = 'content_jsonreset';
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage('plg_content_jsonreset', JPATH_ADMINISTRATOR);
    }

}