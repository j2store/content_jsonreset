<?php
/**
 * --------------------------------------------------------------------------------
 * Content Plugin - Content Json reset
 * --------------------------------------------------------------------------------
 * @package     Joomla 2.5 -  3.x
 * @subpackage  Content
 * @author      Alagesan,
 * @copyright   Copyright (c) 2016 J2Store . All rights reserved.
 * @license     GNU/GPL license: http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
defined('_JEXEC') or die('Restricted access');
?>
<script type="text/javascript">

    function submitJsonForm() {
        (function ($) {
            var favorite = [];
            $.each($("input[name='cid[]']:checked"), function(){
                favorite.push($(this).val());
            });
            if(favorite.length){
                var c = confirm("Are you sure you wish to update?");
                if (c){
                    $('#style-form').submit();
                }
            }else{
                var c = confirm("Please Select Any One ?");
            }
        })(jQuery);
    }
</script>
<span class="pull-right">
    <a class="btn btn-danger btn-large" onclick="submitJsonForm()"><?php echo JText::_('CONTENT_RESET');?> </a>
</span>
<div class="span12">
    <div class="alert alert-message">
        <?php echo JText::_('CONTENT_JSON_FIX_NOTE');?>
    </div>
</div>
<div class="content-json">

    <form action="<?php echo $vars->action; ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal form-validate">
        <input type="hidden" name="option" value="com_plugins"/>
        <input type="hidden" name="view" value="plugin"/>
        <input type="hidden" name="task" value="edit"/>
        <input type="hidden" name="layout" value="edit"/>
        <input type="hidden" name="extension_id" value="<?php echo $vars->id;?>"/>
        <input type="hidden" name="boxchecked" value="0"/>
        <?php echo JHtml::_('form.token'); ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?php echo JText::_('CONTENT_NUM');?></th>
                <th width="20">
                    <input type="checkbox" name="checkall-toggle"
                                      value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>"
                                      onclick="Joomla.checkAll(this)" />
                </th>
                <th><?php echo JText::_('CONTENT_ARTICLE_NAME');?></th>
                <th><?php echo JText::_('CONTENT_COLUMN_NAME');?></th>
                <th><?php echo JText::_('CONTENT_VALUE');?></th>
            </tr>
            </thead>
            <?php if(count($vars->errors) > 0):?>
                <tbody>
                    <?php $i = 1;?>
                    <?php foreach ($vars->errors as $item):?>

                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $checked = JHTML::_('grid.id', $i, $item[$item['primary_key']] );;?></td>
                            <td>
                                <?php echo $item['id'].":".$item['title'];?>

                                <input type="hidden" name="change[<?php echo $i;?>][primary_key_name]" value="<?php echo $item['primary_key'];?>">
                                <input type="hidden" name="change[<?php echo $i;?>][primary_key]" value="<?php echo $item[$item['primary_key']];?>">
                                <input type="hidden" name="change[<?php echo $i;?>][table_name]" value="<?php echo $item['table_name'];?>">
                            </td>
                            <td>
                                <?php echo $item['column_name'];?>
                                <input type="hidden" name="change[<?php echo $i;?>][column_name]" value="<?php echo $item['column_name'];?>">
                            </td>
                            <td>
                                <?php echo $item[$item['column_name']];?>
                                <input type="hidden" name="change[<?php echo $i;?>][column_value]" value="<?php echo $item[$item['column_name']];?>">
                            </td>
                        </tr>
                        <?php $i++;?>
                    <?php endforeach;?>
                </tbody>
            <?php else:?>
                <tfoot>
                <tr>
                    <td colspan="5"><?php echo JText::_('CONTENT_NO_ITEMS_FOUND');?></td>
                </tr>
                </tfoot>
            <?php endif; ?>
        </table>
    </form>
</div>

