<?php
/**
 * --------------------------------------------------------------------------------
 * Content Plugin - Content Json reset
 * --------------------------------------------------------------------------------
 * @package     Joomla 2.5 -  3.x
 * @subpackage  Content
 * @author      Alagesan,
 * @copyright   Copyright (c) 2016 J2Store . All rights reserved.
 * @license     GNU/GPL license: http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://j2store.org
 * --------------------------------------------------------------------------------
 *
 * */
defined('_JEXEC') or die;
/* class JFormFieldFieldtypes extends JFormField */
class JFormFieldJsonfix extends JFormField
{
    protected $type = 'jsonfix';

    public function getInput() {
        
        $lang = JFactory::getLanguage();
        $lang->load('plg_content_jsonreset', JPATH_ADMINISTRATOR);      

        $app = JFactory::getApplication();
        $post = $app->input->getArray($_POST);
        $db     = JFactory::getDbo();
        if(isset($post['change']) && !empty($post['change']) && isset($post['cid']) && !empty($post['cid'])){
            foreach ($post['change'] as $item){
                if( in_array( $item['primary_key'], $post['cid']) ){
                    $query = $db->getQuery(true)
                        ->update('#__'.str_replace( $db->getPrefix(),'', $item['table_name'] ))
                        ->set($item['column_name'].' = "{}"')
                        ->where($item['primary_key_name'].'='.$item['primary_key']);
                    $db->setQuery($query);
                    $db->execute();
                }
            }
        }
        $this->doInitial();
        $results = $this->getJsonTable();
        $error_rows = array();
        if ($results)
        {
            foreach ($results as $result)
            {
                $query = $db->getQuery(true)
                    ->select('*')
                    ->from($result->TABLE_NAME)
                    ->where($result->COLUMN_NAME . ' != "{}"');

                $db->setQuery($query);
                $results = $db->loadAssocList();
                $query = $db->getQuery(true);
                $query = "SHOW KEYS FROM ".'#__'.str_replace($db->getPrefix(), '', $result->TABLE_NAME)." WHERE Key_name = ".$db->q('PRIMARY');
                $db->setQuery($query);
                $keys = $db->loadObject();
                $primary = '';
                if(isset($keys->Column_name)){
                    $primary = $keys->Column_name;
                }

                if ($results)
                {
                    foreach ($results as $row)
                    {
                        if (!$this->is_json($row[$result->COLUMN_NAME]) )
                        {
                            $error = json_last_error_msg();
                            $row['json_error'] = $error;
                            $row['column_name'] = $result->COLUMN_NAME;
                            $row['table_name'] = $result->TABLE_NAME;
                            $row['primary_key'] = $primary;
                            $error_rows[] = $row;
                        }
                    }
                }
            }
        }
        $vars = new JObject();
        $vars->errors = $error_rows;
        $id = $app->input->get('extension_id',0);
        $vars->id = $id;
        $vars->action = "index.php?option=com_plugins&view=plugin&layout=edit&extension_id=".$id;
        $layout = JPATH_SITE."/plugins/content/content_jsonreset/fields/default.php";
        ob_start();
        include($layout);
        $html = ob_get_contents();
        ob_end_clean();
        return  $html;
    }

    function doInitial(){
        //We use this for both checks
        $db     = JFactory::getDbo();
        $results = $this->getJsonTable();
        if ($results)
        {
            foreach ($results as $result)
            {
                $query = $db->getQuery(true)
                    ->update($result->TABLE_NAME)
                    ->set($result->COLUMN_NAME . ' = "{}"')
                    ->where($result->COLUMN_NAME . ' = "" OR ' . $result->COLUMN_NAME . ' = \'{\"\"}\' OR ' . $result->COLUMN_NAME . ' = \'{\\\\\"\\\\\"}\' ');

                $db->setQuery($query);
                $db->execute();
            }
        }
    }

    function getJsonTable(){
        $db     = JFactory::getDbo();
        $config = JFactory::getConfig();
        $query = $db->getQuery(true)
            ->select('TABLE_NAME,COLUMN_NAME')
            ->from('INFORMATION_SCHEMA.COLUMNS')
            ->where('COLUMN_NAME = \'params\' OR COLUMN_NAME = \'attribs\'')
            ->andwhere('TABLE_NAME = '.$db->q($db->getPrefix().'content'))
            ->andWhere('TABLE_SCHEMA = \'' . $config->get('db') . '\'');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    function is_trying_to_be_json($data)
    {
        $data = trim($data);

        return ((substr($data, 0, 1) === '{') && (substr($data, -1, 1) === '}')) ? true : false;
    }

    function is_json()
    {
        call_user_func_array('json_decode', func_get_args());

        return (json_last_error() === JSON_ERROR_NONE);
    }


}
